'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

const Route = use('Route')


Route.group( () => {
  
  Route.resource('projects','ProjectController')
        .apiOnly()
        .validator(new Map([
            [ ['projects.store'],['StoreProject'] ],
            [ ['projects.update'],['UpdateProject'] ]
        ]))

  Route.resource('companies','CompanyController')
        .apiOnly()
        .validator(new Map([
            [ ['companies.store'],['StoreCompany'] ],
            [ ['companies.update'],['UpdateCompany'] ]
        ]))

  Route.resource('companies.profiles','ProfileController')
        .apiOnly()
        .validator(new Map([
            [ ['companies.profiles.store'],['StoreProfile'] ],
            [ ['companies.profiles.update'],['UpdateProfile'] ]
            ]))

  Route.resource('companies.referents','CompanyReferentController')
        .apiOnly()
        .validator(new Map([
            [ ['companies.referents.store'],['StoreReferent'] ],
            [ ['companies.referents.update'],['UpdateReferent'] ]
        ]))

  Route.resource('tags','TagController')
        .apiOnly()
        .validator(new Map([
            [ ['tags.store'],['StoreTag'] ],
            [ ['tags.update'],['UpdateTag'] ]
        ]))

}).prefix('api/v1')
