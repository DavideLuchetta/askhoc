'use strict'

const Project = use('App/Models/Project')
const ProjectFilters = use('App/Filters/ProjectFilters')
const User = use('App/Models/User')

class ProjectController {

  async index ({request}) {

    if ( Object.keys(request.get()).length != 0 ) {
      const filters = ProjectFilters.buildFilters(request.get().filter)
      const projects = Project.query()

      filters.forEach(filter => {
        if (filter.operator == 'in'){
          let values = Array.isArray(filter.value) ? filter.value : [filter.value]
          projects.whereIn( filter.key, values )
        }else{
          projects.where( filter.key, filter.value )
        }
      });

      return await projects.fetch()
    }
    return await Project.query().with('owner').fetch()
    return await Project.all()
  }

  async store ({request,response}) {
    const user = await User.query().with('company').where('_id','5a7b2628bc985741b7f3e3f4').firstOrFail()
    const input = request.only(Project.fillables)
    await Project.create({...input , company: user.company_id})
    return response.route('ProjectController.index')
  }

  async show ({params}) {
    return await Project.findOrFail(params.id)
  }

  async update ({params, request, response}) {
    const project = await Project.findOrFail(params.id)
    const input = request.only(Project.fillables)
    project.merge({...input})
    await project.save()
    return response.route('ProjectController.show',{...params})
  }

  async destroy ({params, response}) {
    await Project.query().where('_id',params.id).delete()
    return response.route('ProjectController.index')
  }
}

module.exports = ProjectController
