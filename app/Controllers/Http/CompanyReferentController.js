'use strict'

const Company = use('App/models/Company')
const User = use('App/Models/User')

class CompanyReferentController {
  async index ({params}) {
    return await Company.query().with('referents').where('_id', params.companies_id).fetch()
  }

  async store ({params, request, response}) {
    const company = await Company.findOrFail(params.companies_id)
    const input = request.only(User.fillables)
    await company.referents().create(input)
    return response.route('CompanyReferentController.index',{...params})
  }
  
  async show ({params}) {
    return await User.findOrFail(params.id)
  }

  async update ({params, request, response}) {
    const user = await User.findOrFail(params.id)
    const input = request.only(User.fillables)
    user.merge({...input})
    await user.save()
    return response.route('CompanyReferentController.show',{...params})
  }

  async destroy ({params, response}) {
    await User.query().where('_id',params.id).delete()
    return response.route('CompanyReferentController.index',{...params})
  }
}

module.exports = CompanyReferentController
