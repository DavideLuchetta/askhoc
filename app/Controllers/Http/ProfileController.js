'use strict'

const Profile = use('App/Models/Profile')
const Company = use('App/Models/Company')

class ProfileController {
  async index ({params}) {
    return await Company.query().with('profile').fetch()
  }

  async store ({params, request, response}) {
    const company = await Company.query().with('profile').where('_id',params.companies_id).first()
    
    if (!company.toJSON().profile) {
      const input = request.only(Profile.fillables)
      await company.profile().create(input)
      return response.route('ProfileController.index',{...params})
    }else{
      return response.status(304).send('Unmodified')
    }
  }

  async show ({params}) {
    return await Profile.findOrFail(params.id)
  }

  async update ({params, request, response}) {
    const profile = await Profile.findOrFail(params.id)
    const input = request.only(Profile.fillables)
    profile.merge({...input})
    await profile.save()
    return response.route('ProfileController.show',{...params})
  }

  async destroy ({params, response}) {
    await Profile.query().where('_id',params.id).delete()
    return response.route('ProfileController.index',{...params})
  }
}

module.exports = ProfileController
