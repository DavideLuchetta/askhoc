'use strict'

const Tag = use('App/Models/Tag')

class TagController {

  async index () {
    return await Tag.all()
  }

  async store ({request, response}) {
    await Tag.create(request.only(Tag.fillables))
    return response.route('TagController.index')
  }

  async show ({params}) {
    return await Tag.findOrFail(params.id)
  }
  
  async update ({params, request, response}) {
    const tag = await Tag.findOrFail(params.id)
    const data = request.only(Tag.fillables)
    tag.merge({...data})
    await tag.save()
    return response.route('TagController.show',{ id: tag.toJSON()._id })
  }

  async destroy ({params, response}) {
    await Tag.query().where('_id',params.id).delete()
    return response.route('TagController.index')
  }

}

module.exports = TagController
