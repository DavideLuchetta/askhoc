'use strict'

const Company = use('App/Models/Company')

class CompanyController {

  async index () {
    return await Company.all()
  }

  async store ({params, request, response}) {
    await Company.create(request.only(Company.fillables))
    return response.route('CompanyController.index')
  }

  async show ({params}) {
    return await Company.findOrFail(params.id)
  }

  async update ({params, request, response}) {
    const company = await Company.findOrFail(params.id)
    const data = request.only(Company.fillables)
    company.merge({...data})
    await company.save()
    return response.route('CompanyController.show',{id : company.toJSON()._id})
  }

  async delete ({params, response}) {
    await Company.query().where('_id',params.id).delete()
    return response.route('CompanyController.index')
  }

}

module.exports = CompanyController
