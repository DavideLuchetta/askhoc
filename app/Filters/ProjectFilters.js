'use strict'

class ProjectFilters {

  static get accepted () {
    return ['owner', 'country','tag','timeframe']
  }

  static buildFilters (filters) {
    const parsed = JSON.parse( filters )
    const result = []
    Object.keys(parsed).map(key => {
      let operator = '='
      if ( ProjectFilters.accepted.includes(key) ) {
        if (key == 'tag'){
          operator = 'in'
        }
        result.push( { key , value: parsed[key] , operator} )
      }
    })
    return result
  }
}


module.exports = ProjectFilters