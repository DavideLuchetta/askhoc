'use strict'

class StoreCompany {

  get rules () {
    return {
      title: 'required|string',
      description: 'required|string'
    }
  }
  
  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }
}

module.exports = StoreCompany
