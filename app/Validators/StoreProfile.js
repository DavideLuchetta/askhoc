'use strict'

class StoreProfile {

  get rules () {
    return {
      name: 'required|string',
      country: 'required|string',
      generalInfo: 'required|string'
    }
  }

  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }
  
}

module.exports = StoreProfile
