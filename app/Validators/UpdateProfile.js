'use strict'

class UpdateProfile {
  
  get rules () {
    return {
      email: 'required|email|unique:users',
      name: 'required|string',
      country: 'required|string',
      generalInfo: 'required|string'
    }
  }

  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }

}

module.exports = UpdateProfile
