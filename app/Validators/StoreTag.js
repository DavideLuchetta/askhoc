'use strict'

class StoreTag {
  
  get rules () {
    return {
      name: 'required|string',
      type: 'required|string|in:techs,skills,industries',
      label: 'required|string|unique:tags'
    }
  }

  get sanitizationRules () {
    return {
      label: 'slug'
    }
  }

  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }

}

module.exports = StoreTag
