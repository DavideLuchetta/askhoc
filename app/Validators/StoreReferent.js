'use strict'

class StoreReferent {
  
  get rules () {
    return {
      email: 'required|email|unique:users',
      name: 'required|string',
      surname: 'required|string',
      role: 'string'
    }
  }

  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }

}

module.exports = StoreReferent
