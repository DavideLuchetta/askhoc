'use strict'

class UpdateReferent {
  
  get rules () {
    return {
      name: 'required|string',
      surname: 'required|string',
      role: 'string'
    }
  }

  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }

}

module.exports = UpdateReferent
