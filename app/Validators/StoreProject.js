'use strict'

class StoreProject {

  get rules () {
    return {
      name: 'required|string',
      due_date: 'required|date',
      description: 'required|string',
      tags: 'required|array',
      contact: 'required|string',
      complexity: 'in:low,medium,high',
      begin: 'date',
      end: 'date'
    }
  }
  
  async fails (errorMessages) {
    return this.ctx.response.send(errorMessages)
  }

}

module.exports = StoreProject
