'use strict'

const Model = use('Model')

class Profile extends Model {
  static get fillables () {
    return ['name','country','generalInfo']
  }

  company () {
    return this.belongsTo('App/Models/Company')
  }
}

module.exports = Profile
