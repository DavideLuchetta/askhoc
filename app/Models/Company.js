'use strict'

const Model = use('Model')

class Company extends Model {

  static get fillables () {
    return ['title','description']
  }

  referents () {
    return this.hasMany('App/Models/User')
  }

  profile () {
    return this.hasOne('App/Models/Profile')
  }
  
}

module.exports = Company
