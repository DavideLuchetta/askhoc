'use strict'

const Model = use('Model')

class Tag extends Model {
  static get fillables () {
    return ['name','type','label']
  }
}

module.exports = Tag
