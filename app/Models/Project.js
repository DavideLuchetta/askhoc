'use strict'

const Model = use('Model')

class Project extends Model {

  static get fillables () {
    return ['name','due_date','description','tags','contact','complexity','begin','end']
  }

  owner () {
    return this.belongsTo('App/Models/Company','company')
  }

  supplier () {
    return this.hasOne('App/Models/Company','_id','supplier_id')
  }

  bidders () {
    return this.belongsToMany('App/Models/Company')
  }

}

module.exports = Project
