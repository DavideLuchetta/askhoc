'use strict'

/*
|--------------------------------------------------------------------------
| TagSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Tag = use('App/Models/Tag')

class TagSeeder {
  async run () {

    await Tag.create({
      name: 'Javscript',
      label: 'js'
    })

    await Tag.create({
      name: 'PHP',
      label: 'php'
    })

    await Tag.create({
      name: 'Ruby on Rails',
      label: 'ror'
    })

  }
}

module.exports = TagSeeder
